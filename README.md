# Baldur's Gate 3 Compendium Backend Project

## Installation

```shell script
composer install --ignore-platform-reqs
```

### Build

```shell script
docker build -f Dockerfile -t bg3c .
```

### RUN

```shell script
docker build -f Dockerfile -t bg3c . && docker run -p 8000:8000 \
-v ./src/modules:/usr/local/romy/web/modules/custom \
-v ./src/profiles:/usr/local/romy/web/profiles/custom \
-v ./src/themes:/usr/local/romy/web/themes/custom \
bg3c
```