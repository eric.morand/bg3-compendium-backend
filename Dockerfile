FROM php:8.1

RUN apt update -y
RUN apt install unzip libpng-dev zlib1g-dev -y

RUN curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
RUN php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN composer --version

RUN docker-php-ext-install gd

WORKDIR /usr/local/romy

RUN composer create-project drupal/recommended-project:^9 .
RUN composer require drupal/field_zip_file:^1.1@alpha
RUN composer require drush/drush

VOLUME /usr/local/romy/web/modules/custom
VOLUME /usr/local/romy/web/profiles/custom
VOLUME /usr/local/romy/web/themes/custom

ADD src/modules/ web/modules/custom
ADD src/profiles/ web/profiles/custom
ADD src/themes/ web/themes/custom
ADD .ht.router.php .ht.router.php

RUN vendor/bin/drush site:install bg3c --site-name="Baldur's Gate 3 Compendium" --db-url=sqlite://sites/default/files/.ht.sqlite --account-pass=holmes
RUN vendor/bin/drush version

RUN ls -la

RUN echo '\
parameters:\n\
  twig.config:\n\
    debug: true\
' > web/sites/default/services.yml

RUN cat web/sites/default/services.yml

RUN composer remove drupal/core-project-message

RUN ls -la web/modules/custom

RUN echo '\
upload_max_filesize = 1G\n\
post_max_size = 1G\n\
memory_limit = -1\n\
max_file_uploads = 1000\n\
' > /usr/local/etc/php/php.ini

ENTRYPOINT ["php", "-S", "0.0.0.0:8000", "-t", "web", ".ht.router.php"]