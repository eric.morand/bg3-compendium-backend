<?php

namespace Drupal\bg3c_core\ViewBuilder;

use Drupal\bg3c_core\Entity\Entry;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

class EntryViewBuilder extends EntityViewBuilder
{
    public function viewMultiple(array $entities = [], $view_mode = 'full', $langcode = NULL)
    {
        $buildResult = [];

        /** @var \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator */
        $fileUrlGenerator = \Drupal::service('file_url_generator');

        /** @var Entry $entry */
        foreach ($entities as $key => $entry) {
            $entry = $this->entityRepository->getTranslationFromContext($entry, $langcode);

            $build = $this->getBuildDefaults($entry, $view_mode);
            $build['#title'] = $entry->getName();
            $build['#mainSpriteSheet'] = [
                'frames' => []
            ];

            $mainSpriteSheet = $entry->getMainSpriteSheet();

            if ($mainSpriteSheet) {
                $frames = $mainSpriteSheet->getFrames();

                foreach ($frames as $frame) {
                    $build['#mainSpriteSheet']['frames'][] = $fileUrlGenerator->generateString($frame->getFileUri());
                }
            }

            $buildResult[$key] = $build;
        }

        return $buildResult;
    }
}