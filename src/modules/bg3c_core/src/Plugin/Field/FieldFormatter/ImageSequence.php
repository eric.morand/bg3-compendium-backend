<?php

namespace Drupal\bg3c_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;

/**
 * Plugin implementation of the 'image_sequence' formatter.
 *
 * @FieldFormatter(
 *   id = "image_sequence",
 *   label = @Translation("Image Sequence"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ImageSequence extends ImageFormatterBase
{
    public static function isApplicable(FieldDefinitionInterface $field_definition)
    {
//        var_dump($field_definition->getTargetBundle()); exit;

        return $field_definition->getTargetEntityTypeId() === 'sprite_sheet';
    }

    /**
     * @inheritDoc
     */
    public function viewElements(\Drupal\Core\Field\FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
        $images = $this->getEntitiesToView($items, $langcode);

        /** @var \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator */
        $fileUrlGenerator = \Drupal::service('file_url_generator');

        /** @var \Drupal\file\FileInterface[] $images */
        foreach ($images as $delta => $image) {
            $elements[$delta] = ['#markup' => $fileUrlGenerator->generateString($image->getFileUri())];
        }

        return $elements;
    }
}