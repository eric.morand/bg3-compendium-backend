<?php

namespace Drupal\bg3c_core\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\bg3c_core\Field\RestaurantTypeRestaurantsItemList;
use Drupal\bg3c_entity\EntityBase;

/**
 * Defines the Entry type entity.
 *
 * @ingroup bg3c_core
 *
 * @BG3CEntityType(
 *   id = "entry_type",
 *   label = @Translation("Entry type"),
 *   label_plural = @Translation("Entry types")
 * )
 */
class EntryType extends EntityBase implements EntryTypeInterface
{
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setDescription(new TranslatableMarkup('The name of the type.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Description'))
      ->setDescription(new TranslatableMarkup('The description of the type.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entries'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Entries'))
      ->setComputed(TRUE)
      ->setTranslatable(FALSE)
      ->setClass(RestaurantTypeRestaurantsItemList::class)
      ->setSetting('target_type', 'entry')
      ->setDescription(new TranslatableMarkup('The entries of that type.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    return $fields;
  }

  /**
   * @return array|string[]
   */
  public function getCacheTagsToInvalidate()
  {
    $restaurantsTags = [];

    foreach ($this->getEntries() as $restaurant) {
      $restaurantsTags = array_merge($restaurantsTags, $restaurant->getCacheTags());
    }

    return Cache::mergeTags(
      parent::getCacheTagsToInvalidate(),
      $restaurantsTags
    );
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->get('name')->value;
  }

  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->set('name', $name);
  }

  /**
   * @return string
   */
  public function getDescription()
  {
    return $this->get('description')->value;
  }

  /**
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->set('description', $description);
  }

  /**
   * @return EntryInterface[]
   */
  public function getEntries()
  {
    $restaurants = [];
    $restaurant_list = $this->get('restaurants');

    foreach ($restaurant_list as $restaurant_item) {
      /** @type EntityReferenceItem $restaurant_item */
      $restaurants[] = $restaurant_item->entity;
    }

    return $restaurants;
  }
}
