<?php
//
//namespace Drupal\bg3c_core\Entity;
//
//use Drupal\bg3c_entity\EntityInterface;
//
///**
// * Provides an interface for defining Review entities.
// *
// * @ingroup bg3c_core
// */
//interface ReviewInterface extends EntityInterface
//{
//  /**
//   * @return string
//   */
//  public function getBody();
//
//  /**
//   * @return float
//   */
//  public function getScore();
//
//  /**
//   * @return integer
//   */
//  public function getRestaurantId();
//
//  /**
//   * @return EntryInterface
//   */
//  public function getRestaurant();
//}
