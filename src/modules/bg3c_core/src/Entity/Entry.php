<?php

namespace Drupal\bg3c_core\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\bg3c_entity\EntityBase;

/**
 * Defines the Entry entity.
 *
 * @ingroup bg3c_core
 *
 * @BG3CEntityType(
 *   id = "entry",
 *   label = @Translation("Entry"),
 *   label_plural = @Translation("Entries"),
 *   handlers = {
 *     "view_builder" = "Drupal\bg3c_core\ViewBuilder\EntryViewBuilder",
 *   },
 * )
 */
class Entry extends EntityBase implements EntryInterface
{
    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['name'] = BaseFieldDefinition::create('string')
            ->setLabel(new TranslatableMarkup('Name'))
            ->setDescription(new TranslatableMarkup('The name of the entry.'))
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE)
            ->setSettings([
                'max_length' => 50,
                'text_processing' => 0,
            ])
            ->setDefaultValue('')
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'string',
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weigth' => 0
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE)
            ->setRequired(TRUE);

//    $fields['address'] = BaseFieldDefinition::create('string_long')
//      ->setLabel(new TranslatableMarkup('Address'))
//      ->setDescription(new TranslatableMarkup('The address of the restaurant.'))
//      ->setRevisionable(TRUE)
//      ->setSettings([
//        'max_length' => 50,
//        'text_processing' => 0,
//      ])
//      ->setDefaultValue('')
//      ->setDisplayOptions('view', [
//        'label' => 'above',
//        'type' => 'string',
//      ])
//      ->setDisplayOptions('form', [
//        'type' => 'string_textarea',
//        'weight' => 25,
//        'settings' => [
//          'rows' => 4,
//        ],
//      ])
//      ->setDisplayConfigurable('form', TRUE)
//      ->setDisplayConfigurable('view', TRUE)
//      ->setRequired(TRUE);

        $fields['main_sprite_sheet'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(new TranslatableMarkup('Main Sprite Sheet'))
            ->setDescription(new TranslatableMarkup('The main sprite sheet of the entry.'))
            ->setSetting('target_type', 'sprite_sheet')
            ->setTranslatable(FALSE)
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        $fields['type'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(new TranslatableMarkup('Type'))
            ->setSetting('target_type', 'entry_type')
            ->setDescription(new TranslatableMarkup('The type of the entry.'))
            ->setRevisionable(TRUE)
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'label'
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        return $fields;
    }

    /**
     * @return array|string[]
     */
    public function getCacheTagsToInvalidate()
    {
        $reviewsTags = [];

//        foreach ($this->getReviews() as $review) {
//            $reviewsTags[] = $review->getCacheTags();
//        }

        return Cache::mergeTags(
            parent::getCacheTagsToInvalidate(),
//            $reviewsTags
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->get('name')->value;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->set('name', $name);
    }

    /**
     * Gets the restaurant address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->get('address')->value;
    }

    /**
     * Sets the restaurant address.
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->set('address', $address);
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->get('rating')->value;
    }

    /**
     * @return SpriteSheet | null
     */
    public function getMainSpriteSheet() {
        /** @var EntityReferenceFieldItemList $fieldItemList */
        $fieldItemList = $this->get('main_sprite_sheet');

        if ($fieldItem = $fieldItemList->get(0)) {
            $spriteSheet = $fieldItem->entity;

            return $spriteSheet;
        }

        return null;
    }

    /**
     * @return ReviewInterface[]
     */
    public function getReviews()
    {
        $reviews = [];
        $reviewsList = $this->get('reviews');

        foreach ($reviewsList as $reviewItem) {
            /** @type EntityReferenceItem $reviewItem */
            $reviews[] = $reviewItem->entity;
        }

        return $reviews;
    }

    /**
     * @return EntryTypeInterface
     */
    public function getType()
    {
        return $this->get('type')->entity;
    }

    /**
     * @param EntryTypeInterface $type
     */
    public function setType(EntryTypeInterface $type)
    {
        $this->set('type', $type);
    }
}
