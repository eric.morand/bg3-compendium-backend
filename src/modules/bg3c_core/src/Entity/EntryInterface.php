<?php

namespace Drupal\bg3c_core\Entity;

use Drupal\bg3c_entity\EntityInterface;

/**
 * Provides an interface for defining Entry entities.
 *
 * @ingroup bg3c_restaurant
 */
interface EntryInterface extends EntityInterface
{
  /**
   * @return string
   */
  public function getName();

  /**
   * @param string $name
   */
  public function setName($name);

  /**
   * @return string
   */
  public function getAddress();

  /**
   * @param string $address
   */
  public function setAddress($address);

  /**
   * @return float
   */
  public function getRating();

  /**
   * @return ReviewInterface[]
   */
  public function getReviews();

  /**
   * @return EntryTypeInterface
   */
  public function getType();

  /**
   * @param EntryTypeInterface $type
   */
  public function setType(EntryTypeInterface $type);
}
