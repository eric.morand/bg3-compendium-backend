<?php

namespace Drupal\bg3c_core\Entity;

use Drupal\bg3c_entity\EntityInterface;

/**
 * Provides an interface for defining Entry type entities.
 *
 * @ingroup bg3c_core
 */
interface EntryTypeInterface extends EntityInterface
{
  /**
   * @return string
   */
  public function getName();

  /**
   * @param string $name
   */
  public function setName($name);

  /**
   * @return string
   */
  public function getDescription();

  /**
   * @param string $description
   */
  public function setDescription($description);

  /**
   * @return EntryInterface[]
   */
  public function getEntries();
}
