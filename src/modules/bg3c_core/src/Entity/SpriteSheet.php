<?php


namespace Drupal\bg3c_core\Entity;

use Drupal\bg3c_entity\EntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Image\Image;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;

/**
 * Defines the Sprite Sheet entity.
 *
 * @ingroup bg3c_core
 *
 * @BG3CEntityType(
 *   id = "sprite_sheet",
 *   label = @Translation("Sprite Sheet"),
 *   label_plural = @Translation("Sprite Sheets")
 * )
 */
class SpriteSheet extends EntityBase
{
    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['name'] = BaseFieldDefinition::create('string')
            ->setLabel(new TranslatableMarkup('Name'))
            ->setDescription(new TranslatableMarkup('The name of the sprite sheet.'))
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE)
            ->setSettings([
                'max_length' => 50,
                'text_processing' => 0,
            ])
            ->setDefaultValue('')
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'string',
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => 0
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE)
            ->setRequired(TRUE);

        $fields['frames'] = BaseFieldDefinition::create('image')
            ->setLabel(new TranslatableMarkup('Frames'))
            ->setTranslatable(FALSE)
            ->setDescription(new TranslatableMarkup('The frames the sprite sheet consists of.'))
            ->setSettings([
                'alt_field' => false
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'file_url_plain',
            ])
            ->setDisplayOptions('form', [
                'weight' => 5
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE)
            ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

        return $fields;
    }

    public function getFoo() {
        return "FOO";
    }

    /**
     * @return File[]
     */
    public function getFrames() {
        $results = [];

        /** @var FileFieldItemList $fieldItemList */
        $fieldItemList = $this->get('frames');

        /** @var EntityReferenceItem $fieldItem */
        foreach ($fieldItemList as $fieldItem) {
            /** @var File $image */
            $image = $fieldItem->entity;

            $results[] = $image;
        }

        return $results;
    }
}