<?php

namespace Drupal\bg3c_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of entities.
 *
 * @ingroup bg3c_entity
 */
class EntityListBuilder extends \Drupal\Core\Entity\EntityListBuilder
{
  /**
   * {@inheritdoc}
   */
  public function buildHeader()
  {
    $header['name'] = $this->t('Name');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity)
  {
    $row['name'] = $entity->toLink($entity->label());

    return $row + parent::buildRow($entity);
  }
}
