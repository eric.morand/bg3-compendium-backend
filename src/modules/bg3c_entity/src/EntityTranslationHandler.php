<?php

namespace Drupal\bg3c_entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for nodes.
 */
class EntityTranslationHandler extends ContentTranslationHandler {

}
