<?php

namespace Drupal\bg3c_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting entities.
 *
 * @ingroup bg3c_entity
 */
class EntityDeleteForm extends ContentEntityDeleteForm {


}
