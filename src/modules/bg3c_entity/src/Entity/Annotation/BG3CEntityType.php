<?php

namespace Drupal\bg3c_entity\Entity\Annotation;

use Drupal\Core\Entity\Annotation\ContentEntityType;

/**
 * Defines a BG3C entity type annotation object.
 *
 * @ingroup bg3c_entity
 *
 * @Annotation
 */
class BG3CEntityType extends ContentEntityType
{
  /**
   * {@inheritdoc}
   */
  public $entity_type_class = 'Drupal\bg3c_entity\Entity\EntityType';
}
