<?php

namespace Drupal\bg3c_entity\Entity;

use Drupal\Core\Entity\ContentEntityType;

class EntityType extends ContentEntityType implements EntityTypeInterface
{

    /** @var array */
    protected $routeNames;

    public function __construct($definition)
    {
        $id = $definition['id'];

        $defaultDefinition['links'] = [
            'canonical' => "/{$id}/{{$id}}",
            'add-form' => "/{$id}/add",
            'edit-form' => "/{$id}/{{$id}}/edit",
            'delete-form' => "/{$id}/{{$id}}/delete",
            'version-history' => "/{$id}/{{$id}}/revisions",
            'revision' => "/{$id}/{{$id}}/revision/{{$id}_revision}",
            'revision_revert' => "/admin/content/{$id}/{{$id}}/revision/{{$id}_revision}/revert",
            'revision_delete' => "/admin/content/{$id}/{{$id}}/revision/{{$id}_revision}/delete",
            'translation_revert' => "/admin/content/{$id}/{{$id}}/revision/{{$id}_revision}/revert/{langcode}",
            'collection' => "/admin/content/{$id}",
        ];

        $defaultDefinition['field_ui_base_route'] = "{$id}.settings";

        $defaultDefinition['handlers'] = [
            'storage' => "Drupal\bg3c_entity\EntityStorage",
            'view_builder' => "Drupal\Core\Entity\EntityViewBuilder",
            'list_builder' => "Drupal\bg3c_entity\EntityListBuilder",
            'views_data' => "Drupal\bg3c_entity\EntityViewsData",
            'translation' => "Drupal\bg3c_entity\EntityTranslationHandler",
            'form' => [
                'default' => "Drupal\bg3c_entity\Form\EntityForm",
                'add' => "Drupal\bg3c_entity\Form\EntityForm",
                'edit' => "Drupal\bg3c_entity\Form\EntityForm",
                'delete' => "Drupal\bg3c_entity\Form\EntityDeleteForm",
            ],
            'access' => "Drupal\bg3c_entity\EntityAccessControlHandler",
            'route_provider' => [
                'html' => "Drupal\bg3c_entity\EntityHtmlRouteProvider",
            ],
        ];

        $defaultDefinition['revision_metadata_keys'] = [
            'revision_user' => 'revision_user',
            'revision_created' => 'revision_created',
            'revision_log_message' => 'revision_log_message',
        ];

        $defaultDefinition['base_table'] = $id;
        $defaultDefinition['data_table'] = "{$id}_field_data";
        $defaultDefinition['revision_table'] = "{$id}_revision";
        $defaultDefinition['revision_data_table'] = "{$id}_field_revision";
        $defaultDefinition['show_revision_ui'] = TRUE;
        $defaultDefinition['translatable'] = TRUE;
        $defaultDefinition['admin_permission'] = "administer {$id} entities";

        $defaultDefinition['entity_keys'] = [
            'id' => 'id',
            'revision' => 'vid',
            'label' => 'name',
            'uuid' => 'uuid',
            'owner' => 'uid',
            'langcode' => 'langcode',
            'status' => 'status',
            'published' => 'status',
        ];

        $this->routeNames = [
            'canonical' => "entity.{$id}.canonical",
        ];

        parent::__construct(array_replace_recursive($defaultDefinition, $definition));
    }
}
