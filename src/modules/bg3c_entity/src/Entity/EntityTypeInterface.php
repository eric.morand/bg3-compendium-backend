<?php


namespace Drupal\bg3c_entity\Entity;


use Drupal\Core\Entity\ContentEntityTypeInterface;

interface EntityTypeInterface extends ContentEntityTypeInterface
{

}
